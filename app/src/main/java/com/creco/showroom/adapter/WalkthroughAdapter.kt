package com.creco.showroom.adapter

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.creco.showroom.R
import com.creco.showroom.model.WizardModel
import com.creco.showroom.ui.main.MainActivity
import com.creco.showroom.ui.user.LoginActivity
import kotlinx.android.synthetic.main.walkthrough_layout.view.*
import java.util.ArrayList

class WalkthroughAdapter(private val context: Context, private val imageModelArrayList: ArrayList<WizardModel>) : PagerAdapter() {

        private val inflater: LayoutInflater


        init {
            inflater = LayoutInflater.from(context)
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

        override fun getCount(): Int {
            return imageModelArrayList.size
        }

        override fun instantiateItem(view: ViewGroup, position: Int): Any {
            val imageLayout = inflater.inflate(R.layout.walkthrough_layout, view, false)!!

            val imageView = imageLayout.findViewById(R.id.image_walkthrough) as ImageView
            val titleWalkthrough = imageLayout.findViewById(R.id.title_walkthrough) as TextView
            val contentWalkthrough = imageLayout.findViewById(R.id.content_walkthrough) as TextView
            val finishtWalkthrough = imageLayout.findViewById(R.id.btn_finish) as TextView

            imageView.setImageResource(imageModelArrayList[position].getImage_drawables())

            if(position == 0) {
                titleWalkthrough.text = "Beli mobil jadi mudah"
                contentWalkthrough.text = "Dapatkan info menarik hanya di creco"
                finishtWalkthrough.visibility = View.INVISIBLE
            } else if (position == 1) {
                titleWalkthrough.text = "Dapatkan cashback hingga 20%"
                contentWalkthrough.text = "Yuk langsung kunjungi websitenya"
                finishtWalkthrough.visibility = View.INVISIBLE
            } else if (position == 2) {
                titleWalkthrough.text = "Cicilan mudah dan murah"
                contentWalkthrough.text = "Sumpah ini beneran"
                finishtWalkthrough.visibility = View.VISIBLE
            }

            view.addView(imageLayout, 0)

            view.btn_finish.setOnClickListener {
                val page = Intent(context, LoginActivity::class.java)

                context.startActivity(page)
            }

            return imageLayout
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view == `object`
        }

        override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

        override fun saveState(): Parcelable? {
            return null
        }

}
