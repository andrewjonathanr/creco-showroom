package com.creco.showroom.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.creco.showroom.R
import com.creco.showroom.model.MessageDetailModel
import com.creco.showroom.ui.general.GeneralWebViewActivity

class NotificationAdapter(var notificationList: List<MessageDetailModel>) : RecyclerView.Adapter<NotificationAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_notification, parent, false) as View

        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val data = notificationList[position]
        var context = holder.title.context

        holder.title.text = data.title
        holder.date.text = data.created_at

//        holder.root.setOnClickListener {
//            val page = Intent(context, GeneralWebViewActivity::class.java)
//
//            page.putExtra("detailRepository", data)
//            context.startActivity(page)
//        }
    }


    override fun getItemCount(): Int {
        return notificationList.size
    }

    class Holder(view : View) : RecyclerView.ViewHolder(view) {
        val root = view.findViewById<LinearLayout>(R.id.lv_notification)
        val title = view.findViewById<TextView>(R.id.tv_notification_title)
        val date = view.findViewById<TextView>(R.id.tv_notification_date)
    }

}
