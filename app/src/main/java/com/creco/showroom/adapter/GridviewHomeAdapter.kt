package com.creco.showroom.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.creco.showroom.R
import com.creco.showroom.model.HomeMenu
import kotlinx.android.synthetic.main.gridview_home_menus.view.*

class GridviewHomeAdapter : BaseAdapter {
    var menus = ArrayList<HomeMenu>()
    var context: Context? = null

    constructor(context: Context?, menus: ArrayList<HomeMenu>) : super() {
        this.context = context
        this.menus = menus
    }

    override fun getCount(): Int {
        return menus.size
    }

    override fun getItem(position: Int): Any {
        return menus[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val food = this.menus[position]

        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val menuView = inflator.inflate(R.layout.gridview_home_menus, null)
        menuView.imgHomeMenu.setImageResource(food.image!!)
        menuView.tvName.text = food.name!!

        return menuView
    }

}