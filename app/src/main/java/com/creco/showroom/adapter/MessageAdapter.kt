package com.creco.showroom.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.creco.showroom.R
import com.creco.showroom.model.MessageDetailModel
import com.creco.showroom.ui.message.message.MessageDetailActivity

class MessageAdapter(var messageList: List<MessageDetailModel>) : RecyclerView.Adapter<MessageAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_message, parent, false) as View

        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val data = messageList[position]
        var context = holder.title.context

        var img_url = data.image_url

        holder.title.text = data.title
//        holder.desc.text = data.body
        holder.date.text = data.created_at

        Glide.with(holder.img.context)
            .load(img_url)
            //.apply(RequestOptions().override(190,190))
            .into(holder.img)

        holder.root.setOnClickListener {
            val page = Intent(context, MessageDetailActivity::class.java)

            page.putExtra("detailMessage", data)
            context.startActivity(page)

        }


    }


    override fun getItemCount(): Int {
        return messageList.size
    }

    class Holder(view : View) : RecyclerView.ViewHolder(view) {
        val root = view.findViewById<LinearLayout>(R.id.lv_message)

        val title = view.findViewById<TextView>(R.id.tv_message_title)
        //        val desc = view.findViewById<TextView>(R.id.tv_message_desc)
        val date = view.findViewById<TextView>(R.id.tv_message_date)
        val img = view.findViewById<ImageView>(R.id.iv_message_picture)
    }

}
