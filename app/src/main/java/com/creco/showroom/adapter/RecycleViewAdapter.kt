package com.creco.showroom.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.creco.showroom.R
import com.creco.showroom.model.HistoryModel
import kotlinx.android.synthetic.main.listview_history_item.view.*
import java.text.NumberFormat
import java.util.*

class RecycleViewAdapter(private val context: Context, private val item: List<HistoryModel>, private val listener:(HistoryModel)-> Unit)
    : RecyclerView.Adapter<RecycleViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.listview_history_item, parent, false))

    override fun getItemCount():Int {

        return item.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(item[position], listener, context)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindItem(historyModel: HistoryModel, listener: (HistoryModel) -> Unit, context: Context) {
            val nf = NumberFormat.getInstance(Locale.US)

            itemView.tv_title_history.text = historyModel.name
            itemView.tv_caption_history.text = historyModel.caption

            itemView.setOnClickListener {
                listener(historyModel)
            }
        }
    }


}