package com.creco.showroom.ui.message.message

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import com.creco.showroom.R
import com.creco.showroom.adapter.MessageAdapter
import com.creco.showroom.base.BaseFragment
import com.creco.showroom.model.MessageDetailModel
import com.creco.showroom.model.MessageModel
import com.creco.showroom.network.Api
import com.creco.showroom.util.DateUtils
import kotlinx.android.synthetic.main.fragment_message.*
import kotlinx.android.synthetic.main.message_receive.view.*
import kotlinx.android.synthetic.main.message_send.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MessageFragment : BaseFragment() {

    override fun contentView(): Int {
        return R.layout.fragment_message
    }

    override fun onCreated(view: View) {
        rv_list_message.layoutManager = LinearLayoutManager(context)
        rv_list_message.adapter = MessageAdapter(listOf<MessageDetailModel>())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loadMessages()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun loadMessages() {
        Api.invoke().getMessage().enqueue(object : Callback<MessageModel> {
            override fun onFailure(call: Call<MessageModel>, t: Throwable) {
                Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<MessageModel>, response: Response<MessageModel>) {
                if (response.isSuccessful) {
                    showMessages(response.body()!!)
                } else {
                    Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun showMessages(Message: MessageModel) {
        rv_list_message.adapter = MessageAdapter(Message.data_inbox)
    }
}

