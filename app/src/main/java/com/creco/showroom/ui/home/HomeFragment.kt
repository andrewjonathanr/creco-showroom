package com.creco.showroom.ui.home

import android.support.design.widget.FloatingActionButton
import android.view.View
import com.creco.showroom.R
import com.creco.showroom.adapter.GridviewHomeAdapter
import com.creco.showroom.base.BaseFragment
import com.creco.showroom.model.HomeMenu
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.find
import org.jetbrains.anko.support.v4.toast

class HomeFragment : BaseFragment(), View.OnClickListener {
    var adapter: GridviewHomeAdapter? = null
    var menusList = ArrayList<HomeMenu>()
    private lateinit var swipeFab: FloatingActionButton
    private lateinit var alphaFab: FloatingActionButton
    private lateinit var betaFab: FloatingActionButton


    override fun contentView(): Int {
        return R.layout.fragment_home
    }

    override fun onCreated(view: View) {
        swipeFab = view.find(R.id.swipe_button)
        alphaFab = view.find(R.id.fab_alpha)
        betaFab = view.find(R.id.fab_beta)

        // load foods
        menusList.add(HomeMenu("Alpha", R.drawable.ic_launcher_background))
        menusList.add(HomeMenu("Beta", R.drawable.ic_launcher_background))
        menusList.add(HomeMenu("Gamma", R.drawable.ic_launcher_background))
        menusList.add(HomeMenu("Delta",R.drawable.ic_launcher_background))
        menusList.add(HomeMenu("Epsilon", R.drawable.ic_launcher_background))
        menusList.add(HomeMenu("Zeta", R.drawable.ic_launcher_background))
        adapter = GridviewHomeAdapter(context, menusList)

        gv_home_menus.adapter = adapter

        swipeFab.setOnClickListener(this)
        alphaFab.setOnClickListener(this)
        betaFab.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.swipe_button-> {
                toast("test")
            }

            R.id.fab_alpha-> {
                toast("alpha test")
            }

            R.id.fab_beta-> {
                toast("beta test")
            }

        }
    }
}