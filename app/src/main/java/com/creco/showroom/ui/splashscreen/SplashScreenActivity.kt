package com.creco.showroom.ui.splashscreen

import android.os.Handler
import android.view.WindowManager
import com.creco.showroom.R
import com.creco.showroom.base.BaseActivity
import com.bumptech.glide.Glide
import com.creco.showroom.ui.walkthrough.WalkthroughActivity
import kotlinx.android.synthetic.main.activity_splash_screen.iv_splass
import org.jetbrains.anko.startActivity

class SplashScreenActivity : BaseActivity() {

    override fun contentView(): Int {
        return R.layout.activity_splash_screen
    }

    override fun onCreated() {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)


        Glide.with(this).load(R.mipmap.ic_launcher).into(iv_splass)

        Handler().postDelayed({
                startActivity<WalkthroughActivity>()
                finish()
        }, 2000)
    }
}