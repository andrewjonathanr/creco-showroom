package com.creco.showroom.ui.message.notification

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast

import com.creco.showroom.R
import com.creco.showroom.adapter.NotificationAdapter
import com.creco.showroom.adapter.RecycleViewAdapter
import com.creco.showroom.base.BaseFragment
import com.creco.showroom.model.HistoryModel
import com.creco.showroom.model.MessageDetailModel
import com.creco.showroom.model.MessageModel
import com.creco.showroom.network.Api
import kotlinx.android.synthetic.main.fragment_notification.*
import org.jetbrains.anko.find
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NotificationFragment : BaseFragment() {

    override fun contentView(): Int {
        return R.layout.fragment_notification
    }

    override fun onCreated(view: View) {
        rv_list_notification.layoutManager = LinearLayoutManager(context)
        rv_list_notification.adapter = NotificationAdapter(listOf<MessageDetailModel>())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loadNotications()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun loadNotications() {
        Api.invoke().getNotification().enqueue(object : Callback<MessageModel> {
            override fun onFailure(call: Call<MessageModel>, t: Throwable) {
                Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<MessageModel>, response: Response<MessageModel>) {
                if (response.isSuccessful) {
                    showNotifications(response.body()!!)
                } else {
                    Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun showNotifications(Notification: MessageModel) {
        rv_list_notification.adapter = NotificationAdapter(Notification.data_inbox)
    }
}