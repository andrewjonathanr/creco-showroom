package com.creco.showroom.ui.profile

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.creco.showroom.R
import com.creco.showroom.base.BaseActivity

class DokumenReminderActivity : BaseActivity() {

    override fun contentView(): Int {
        return R.layout.activity_dokumen_reminder
    }

    override fun onCreated() {
        setContentView(R.layout.activity_dokumen_reminder)

        val actionbar = supportActionBar

        actionbar!!.title = "Pengingat Dokumen"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
