package com.creco.showroom.ui.calculator

import android.view.View
import com.creco.showroom.R
import com.creco.showroom.base.BaseFragment

class CalculatorFragment : BaseFragment() {

    override fun contentView(): Int {
        return R.layout.fragment_calculator
    }

    override fun onCreated(view: View) {

    }
}