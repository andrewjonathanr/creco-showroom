package com.creco.showroom.ui.main

import android.content.Intent
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import com.creco.showroom.R
import com.creco.showroom.base.BaseActivity
import com.creco.showroom.ui.calculator.CalculatorFragment
import com.creco.showroom.ui.home.HomeFragment
import com.creco.showroom.ui.message.InboxFragment
import com.creco.showroom.ui.product.ProductFragment
import com.creco.showroom.ui.profile.ProfileFragment
import com.creco.showroom.util.TypefaceUtil
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.noButton
import org.jetbrains.anko.yesButton

class MainActivity : BaseActivity() {

    private lateinit var fragment: Fragment
    private var viewIsAtHome: Boolean = false
    private lateinit var menu: Menu

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val itemId = item.itemId

        loadFragment(itemId)
        Log.d("TEST", "TEST")

        true
    }

    override fun contentView(): Int {
        return R.layout.activity_main
    }

    override fun onCreated() {
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        menu = navigation.menu
        menu.add(Menu.NONE, R.id.navigation_beranda, Menu.NONE,
            getString(R.string.beranda)).setIcon(R.drawable.ic_home_24dp)
        menu.add(Menu.NONE, R.id.navigation_produk, Menu.NONE,
            getString(R.string.produk)).setIcon(R.drawable.ic_history_24dp)
        menu.add(Menu.NONE, R.id.navigation_kalkulator, Menu.NONE,
            getString(R.string.kalkulator)).setIcon(R.drawable.ic_calc_24dp)
        menu.add(Menu.NONE, R.id.navigation_inbox, Menu.NONE,
            getString(R.string.inbox)).setIcon(R.drawable.ic_notifications_24dp)
        menu.add(Menu.NONE, R.id.navigation_profile, Menu.NONE,
            getString(R.string.profile)).setIcon(R.drawable.ic_settings_24dp)

        loadFragment(R.id.navigation_beranda)

    }

    private fun loadFragment(itemId: Int) {
        when(itemId) {
            R.id.navigation_beranda -> {
                fragment = HomeFragment()
                viewIsAtHome = true
            }

            R.id.navigation_produk -> {
                fragment = ProductFragment()
                viewIsAtHome = false
            }

            R.id.navigation_kalkulator -> {
                fragment = CalculatorFragment()
                viewIsAtHome = false
            }

            R.id.navigation_inbox -> {
                fragment = InboxFragment()
                viewIsAtHome = false
            }

            R.id.navigation_profile -> {
                fragment = ProfileFragment()
                viewIsAtHome = false
            }

        }

        val fragmentManager =  supportFragmentManager
        val ft = fragmentManager.beginTransaction()
        ft.replace(R.id.frame_layout, fragment)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            alert("Apakah Anda Yakin Keluar ?") {
                yesButton {
                    val intent = Intent(Intent.ACTION_MAIN)
                    intent.addCategory(Intent.CATEGORY_HOME)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    finish()
                }

                noButton {
                    //it.dismiss()
                }
            }.show().setCancelable(false)
        }
        return super.onKeyDown(keyCode, event)
    }

//    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        var requestCode = requestCode
//        requestCode = requestCode and 0xffff
//        for (fragment in supportFragmentManager.fragments) {
//            fragment?.onActivityResult(requestCode, resultCode, data)
//        }
//        super.onActivityResult(requestCode, resultCode, data)
//    }
}
