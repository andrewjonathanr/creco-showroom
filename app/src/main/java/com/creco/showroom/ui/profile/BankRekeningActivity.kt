package com.creco.showroom.ui.profile

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.creco.showroom.R
import com.creco.showroom.base.BaseActivity

class BankRekeningActivity : BaseActivity() {
    override fun contentView(): Int {
        return R.layout.activity_bank_rekening
    }

    override fun onCreated() {
        setContentView(R.layout.activity_bank_rekening)

        val actionbar = supportActionBar

        actionbar!!.title = "Bank Rekening"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
