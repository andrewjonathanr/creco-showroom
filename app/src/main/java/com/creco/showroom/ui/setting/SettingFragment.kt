package com.creco.showroom.ui.setting

import android.view.View
import com.creco.showroom.R
import com.creco.showroom.base.BaseFragment

class SettingFragment : BaseFragment() {

    override fun contentView(): Int {
        return R.layout.fragment_setting
    }

    override fun onCreated(view: View) {

    }
}