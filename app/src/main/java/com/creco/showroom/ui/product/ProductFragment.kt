package com.creco.showroom.ui.product

import android.support.v4.app.Fragment
import android.view.View
import com.creco.showroom.R
import com.creco.showroom.base.BaseFragment

class ProductFragment : BaseFragment() {
    override fun contentView(): Int {
        return R.layout.fragment_product
    }

    override fun onCreated(view: View) {

    }

}
