package com.creco.showroom.ui.user

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.creco.showroom.R
import com.creco.showroom.base.BaseActivity
import com.creco.showroom.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class RegisterActivity : BaseActivity() {
    override fun contentView(): Int {
        return R.layout.activity_register
    }

    override fun onCreated() {
        btn_register.setOnClickListener {
            val page = Intent(this, MainActivity::class.java)

            this!!.startActivity(page)
        }
    }
}
