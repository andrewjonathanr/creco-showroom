package com.creco.showroom.ui.message.message

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.creco.showroom.R
import com.creco.showroom.model.MessageDetailModel

class MessageDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_message)

        val data= intent.getSerializableExtra("detailMessage") as MessageDetailModel
        val actionbar = supportActionBar

        actionbar!!.title = "Pesan"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val title = findViewById<TextView>(R.id.tv_detail_message_title)
        val date = findViewById<TextView>(R.id.tv_detail_message_date)
        val desc = findViewById<TextView>(R.id.tv_detail_message_desc)
        val image = findViewById<ImageView>(R.id.iv_detail_message_image)

        title.text = data.title
        date.text = data.created_at
        desc.text = data.body

        Glide.with(image.context)
            .load(data.image_url)
            .into(image)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
