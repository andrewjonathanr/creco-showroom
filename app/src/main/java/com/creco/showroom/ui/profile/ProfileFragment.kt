package com.creco.showroom.ui.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.creco.showroom.BuildConfig
import com.creco.showroom.R
import com.creco.showroom.base.BaseFragment
import com.creco.showroom.ui.general.GeneralWebViewActivity
import com.creco.showroom.ui.user.LoginActivity
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment() {

    override fun contentView(): Int {
        return R.layout.fragment_profile
    }

    override fun onCreated(view: View) {
        btn_profile_data_diri.setOnClickListener {
            val page = Intent(context, DataDiriActivity::class.java)

            context!!.startActivity(page)
        }

        btn_profile_bank_rekening.setOnClickListener {
            val page = Intent(context, BankRekeningActivity::class.java)

            context!!.startActivity(page)
        }

        btn_profile_dokumen.setOnClickListener {
            val page = Intent(context, DokumenReminderActivity::class.java)

            context!!.startActivity(page)
        }

        btn_profile_syarat_dan_ketentuan.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title", "Syarat dan Ketentuan")
            bundle.putString("site_url", "https://www.gojek.com/privacy-policies/")

            val page = Intent(context, GeneralWebViewActivity::class.java)

            page.putExtras(bundle)
            startActivity(page)
        }

        btn_profile_kebijakan_privasi.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title", "Kebijakan Privasi")
            bundle.putString("site_url", "https://www.gojek.com/privacy-policies/")

            val page = Intent(context, GeneralWebViewActivity::class.java)

            page.putExtras(bundle)
            startActivity(page)
        }

        btn_profile_pusat_bantuan.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title", "Pusat Bantuan")
            bundle.putString("site_url", "https://www.tokopedia.com/help/article/cara-bayar-dengan-transfer-bank?refid=t-0055")

            val page = Intent(context, GeneralWebViewActivity::class.java)

            page.putExtras(bundle)
            startActivity(page)
        }

        btn_profile_rate.setOnClickListener {
            val uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)
            var intent = Intent(Intent.ACTION_VIEW, uri)
            // To count with Play market backstack, After pressing back button,
            // to taken back to our application, we need to add following flags to intent.
            intent.addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK
            )

            if (intent.resolveActivity(activity!!.packageManager) != null) {
                startActivity(intent)
            } else {
                intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)
                )
                if (intent.resolveActivity(activity!!.packageManager) != null) {
                    startActivity(intent)
                } else {
                    Toast.makeText(activity, "No play store or browser app", Toast.LENGTH_LONG)
                        .show()
                }
            }
        }

        btn_signout.setOnClickListener {
            val page = Intent(context, LoginActivity::class.java)

            context!!.startActivity(page)
        }
    }
}