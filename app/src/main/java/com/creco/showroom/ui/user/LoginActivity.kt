package com.creco.showroom.ui.user

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.creco.showroom.R
import com.creco.showroom.base.BaseActivity
import com.creco.showroom.ui.main.MainActivity
import com.creco.showroom.ui.profile.DataDiriActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_profile.*

class LoginActivity : BaseActivity() {
    override fun contentView(): Int {
        return R.layout.activity_login
    }

    override fun onCreated() {
        btn_login.setOnClickListener {
            val page = Intent(this, MainActivity::class.java)

            this!!.startActivity(page)
        }

        btn_register.setOnClickListener {
            val page = Intent(this, RegisterActivity::class.java)

            this!!.startActivity(page)
        }
    }

}
