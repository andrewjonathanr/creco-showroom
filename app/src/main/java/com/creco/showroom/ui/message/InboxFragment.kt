package com.creco.showroom.ui.message

import android.graphics.Color
import android.support.v4.app.FragmentActivity
import android.view.View
import android.widget.Button
import com.creco.showroom.R
import com.creco.showroom.adapter.InboxAdapter
import com.creco.showroom.base.BaseFragment
import com.creco.showroom.ui.message.message.MessageFragment
import com.creco.showroom.ui.message.notification.NotificationFragment
import kotlinx.android.synthetic.main.fragment_inbox.*


class InboxFragment : BaseFragment(), View.OnClickListener {

    override fun contentView(): Int {
        return R.layout.fragment_inbox
    }

    override fun onCreated(view: View) {
        val adapter = InboxAdapter((activity as FragmentActivity).supportFragmentManager)
        adapter.addFragment(NotificationFragment(), "Pemberitahuan")
        adapter.addFragment(MessageFragment(), "Pesan")

        v_pager_inbox.adapter = adapter
        tabs_inbox.setupWithViewPager(v_pager_inbox)

        tabs_inbox.setSelectedTabIndicatorColor(Color.parseColor("#1276B1"));
        tabs_inbox.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#111111"));
    }

    override fun onClick(v: View?) {

    }
}