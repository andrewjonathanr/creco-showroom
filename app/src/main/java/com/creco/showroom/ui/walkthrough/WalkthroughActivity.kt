package com.creco.showroom.ui.walkthrough

import android.support.v4.view.ViewPager
import com.creco.showroom.R
import com.creco.showroom.adapter.WalkthroughAdapter
import com.creco.showroom.base.BaseActivity
import com.creco.showroom.model.WizardModel
import kotlinx.android.synthetic.main.activity_wizard.*
import kotlin.collections.ArrayList

class WalkthroughActivity : BaseActivity() {

    private var imageModelArrayList: ArrayList<WizardModel>? = null
    private val myImageList = intArrayOf(R.drawable.img_mobil_walkthrough, R.drawable.img_mobil_walkthrough, R.drawable.img_mobil_walkthrough)
    private val waltkthroug_title = intArrayOf(1, 2, 3)

    override fun contentView(): Int {
        return R.layout.activity_wizard
    }

    override fun onCreated() {
        //imageModelArrayList = ArrayList()
        imageModelArrayList = populateList()

        init()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun populateList(): ArrayList<WizardModel> {

        val list = ArrayList<WizardModel>()
        var s = myImageList.size - 1

        for (i in 0..s) {
            val imageModel = WizardModel()
            imageModel.setImage_drawables(myImageList[i])
            list.add(imageModel)
        }

        return list
    }

    private fun init() {
        var s = myImageList.size - 1

        v_pager_wizard.adapter = WalkthroughAdapter(v_pager_wizard.context, this.imageModelArrayList!!)

        v_pager_indicator.setViewPager(v_pager_wizard)

        val density = resources.displayMetrics.density

        v_pager_indicator.setRadius(5 * density)

        NUM_PAGES = imageModelArrayList!!.size

        v_pager_indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {

            }
        })

    }

    companion object {
        private var pager: ViewPager? = null
        private var currentPage = 0
        private var NUM_PAGES = 0
    }
}
