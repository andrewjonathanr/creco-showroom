package com.creco.showroom.ui.profile

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.creco.showroom.R
import com.creco.showroom.base.BaseActivity
import com.creco.showroom.ui.general.GeneralWebViewActivity
import kotlinx.android.synthetic.main.activity_general_webview.*
import kotlinx.android.synthetic.main.fragment_profile.*

class DataDiriActivity : BaseActivity() {
    override fun contentView(): Int {
        return R.layout.activity_data_diri
    }

    override fun onCreated() {
        setContentView(R.layout.activity_data_diri)

        val actionbar = supportActionBar

        actionbar!!.title = "Data Diri"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
