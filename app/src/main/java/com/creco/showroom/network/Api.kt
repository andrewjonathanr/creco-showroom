package com.creco.showroom.network

import com.creco.showroom.BuildConfig.BASE_URL
import com.creco.showroom.model.MessageModel
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Api {
    @GET("api/get_inbox/1")
    fun getMessage(): Call<MessageModel>

    @GET("api/get_inbox/2")
    fun getNotification(): Call<MessageModel>

    companion object {
        operator fun invoke(): Api {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api::class.java)
        }
    }
}