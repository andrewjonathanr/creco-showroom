package com.creco.showroom.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.creco.showroom.util.TypefaceUtil

abstract class BaseActivity : AppCompatActivity() {
    abstract fun contentView(): Int
    abstract fun onCreated()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        TypefaceUtil.overrideFont(
            getApplicationContext(),
            "SERIF",
            "fonts/muesosans.ttf"
        );

        setContentView(contentView())
        onCreated()
    }

    fun setPreventActionUI(){
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun clearPreventActionUI(){
        this.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
}