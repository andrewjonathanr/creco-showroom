package com.creco.showroom.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivityMaps : AppCompatActivity() {
    abstract fun contentView(): Int
    abstract fun onCreated(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentView())
        onCreated(savedInstanceState)

    }

}