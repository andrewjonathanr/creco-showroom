package com.creco.showroom.base

interface BaseView {
    fun showLoading()
    fun hideLoading()
}