package com.creco.showroom.model

import java.io.Serializable

data class MessageDetailModel(
    val body: String,
    val created_at: String,
    val created_by: String,
    val id: String,
    val image_url: String,
    val status: String,
    val title: String,
    val type: String
) : Serializable