package com.creco.showroom.model

class HistoryModel {
    var name: String? = null
    var image: Int? = null
    var caption: String? = null

    constructor(name: String, image: Int?, caption: String) {
        this.name = name
        this.image = image
        this.caption = caption
    }
}