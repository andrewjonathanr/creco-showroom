package com.creco.showroom.model

import java.io.Serializable

data class MessageModel(
    val data_inbox: List<MessageDetailModel>,
    val message: String,
    val status_code: String
) : Serializable